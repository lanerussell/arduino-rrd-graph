#!/usr/bin/python3
# Author: Lane Russell
# This program captures serial port output from an Arduino
# temperature sensor and logs it to an RRD file.

import datetime, serial, subprocess, threading, time

def update_rrd(rrdstring):
    x = 0
    while x < 8:
        try:
            subprocess.check_output(rrdstring, shell=True)
        except:
            x += 1
            print(rrdstring)
            time.sleep(0.5)
            continue
        break

if __name__ == "__main__":
    baud_rate = 9600
    serial_port = '/dev/ttyUSB0'

    listener = serial.Serial(serial_port, baud_rate, timeout=1)
    time.sleep(5)

    align_timer = (0.5 - (time.time() % 1))
    if align_timer > 0.0:
        time.sleep(align_timer)
    else:
        time.sleep(1 - abs(align_timer))
        
    start_time = time.time()    
    
    while True:
        listener.flushInput()
        listener.flushOutput()

        listener.write(b'\n')
        serial_read = str(listener.readline().decode()).rstrip()

        if serial_read != '':
            data_array = serial_read.split(',')
            rrdstring = ''
            rrdstring += '/usr/bin/rrdtool '
            rrdstring += 'update /opt/environment/environment.rrd '
            rrdstring += str(int(time.time()))

            for i in data_array:
                rrdstring += ':' + i

            thread = threading.Thread(target=update_rrd, args=(rrdstring,))
            thread.start()

            time.sleep(5 - (time.time() - start_time) % 5)
