#!/bin/bash
  
rrdtool create environment.rrd \
--start N --step 5 \
DS:instant_temp:GAUGE:5:U:U \
DS:ema_temp:GAUGE:5:U:U \
DS:instant_hum:GAUGE:5:U:U \
DS:ema_hum:GAUGE:5:U:U \
RRA:LAST:0.5:1:720 \
RRA:AVERAGE:0.5:24:720 \
RRA:AVERAGE:0.5:168:720 \
RRA:AVERAGE:0.5:720:720 \
RRA:AVERAGE:0.5:8640:720

