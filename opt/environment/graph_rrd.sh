#!/bin/bash

#sleep 10

graph_layout="--width 697 \
--height 290 \
--full-size-mode \
--alt-autoscale \
--left-axis-formatter numeric \
--left-axis-format %5.1lf"


rrdtool graph /var/www/environment/images/1h.png -a PNG \
--title="Temperature" \
--start -1h \
${graph_layout} \
'DEF:instant_temp=/opt/environment/environment.rrd:instant_temp:AVERAGE' \
'DEF:ema_temp=/opt/environment/environment.rrd:ema_temp:AVERAGE' \
'LINE1:instant_temp#eac8da:Instant Temp' \
'GPRINT:instant_temp:AVERAGE:   Avg\:%5.2lf °C  ' \
'GPRINT:instant_temp:MAX:Max\:%5.2lf °C  ' \
'GPRINT:instant_temp:MIN:Min\:%5.2lf °C  ' \
'GPRINT:instant_temp:LAST:Now\:%5.2lf °C  \n' \
'LINE2:ema_temp#cc79a7:EMA Temp' \
'GPRINT:ema_temp:AVERAGE:       Avg\:%5.2lf °C  ' \
'GPRINT:ema_temp:MAX:Max\:%5.2lf °C  ' \
'GPRINT:ema_temp:MIN:Min\:%5.2lf °C  ' \
'GPRINT:ema_temp:LAST:Now\:%5.2lf °C\n'

rrdtool graph /var/www/environment/images/1h_hum.png -a PNG \
--title="Humidity" \
--start -1h \
${graph_layout} \
'DEF:instant_hum=/opt/environment/environment.rrd:instant_hum:AVERAGE' \
'DEF:ema_hum=/opt/environment/environment.rrd:ema_hum:AVERAGE' \
'LINE1:instant_hum#a5d6f3:Instant Hum' \
'GPRINT:instant_hum:AVERAGE:   Avg\:%5.2lf %%  ' \
'GPRINT:instant_hum:MAX:Max\:%5.2lf %%  ' \
'GPRINT:instant_hum:MIN:Min\:%5.2lf %%  ' \
'GPRINT:instant_hum:LAST:Now\:%5.2lf %%  \n' \
'LINE2:ema_hum#56b4e9:EMA Hum' \
'GPRINT:ema_hum:AVERAGE:       Avg\:%5.2lf %%  ' \
'GPRINT:ema_hum:MAX:Max\:%5.2lf %%  ' \
'GPRINT:ema_hum:MIN:Min\:%5.2lf %%  ' \
'GPRINT:ema_hum:LAST:Now\:%5.2lf %%\n'


rrdtool graph /var/www/environment/images/1d.png -a PNG \
--title="Temperature" \
${graph_layout} \
'DEF:ema_temp=/opt/environment/environment.rrd:ema_temp:AVERAGE' \
'LINE2:ema_temp#cc79a7:EMA Temp' \
'GPRINT:ema_temp:AVERAGE:       Avg\:%5.2lf °C  ' \
'GPRINT:ema_temp:MAX:Max\:%5.2lf °C  ' \
'GPRINT:ema_temp:MIN:Min\:%5.2lf °C  ' \
'GPRINT:ema_temp:LAST:Now\:%5.2lf °C\n'

rrdtool graph /var/www/environment/images/1d_hum.png -a PNG \
--title="Humidity" \
${graph_layout} \
'DEF:ema_hum=/opt/environment/environment.rrd:ema_hum:AVERAGE' \
'LINE2:ema_hum#56b4e9:EMA Hum' \
'GPRINT:ema_hum:AVERAGE:       Avg\:%5.2lf %%  ' \
'GPRINT:ema_hum:MAX:Max\:%5.2lf %%  ' \
'GPRINT:ema_hum:MIN:Min\:%5.2lf %%  ' \
'GPRINT:ema_hum:LAST:Now\:%5.2lf %%\n'


rrdtool graph /var/www/environment/images/7d.png -a PNG \
--title="Temperature" \
--start -7d \
${graph_layout} \
'DEF:ema_temp=/opt/environment/environment.rrd:ema_temp:AVERAGE' \
'LINE2:ema_temp#cc79a7:EMA Temp' \
'GPRINT:ema_temp:AVERAGE:       Avg\:%5.2lf °C  ' \
'GPRINT:ema_temp:MAX:Max\:%5.2lf °C  ' \
'GPRINT:ema_temp:MIN:Min\:%5.2lf °C  ' \
'GPRINT:ema_temp:LAST:Now\:%5.2lf °C\n'

rrdtool graph /var/www/environment/images/7d_hum.png -a PNG \
--title="Humidity" \
--start -7d \
${graph_layout} \
'DEF:ema_hum=/opt/environment/environment.rrd:ema_hum:AVERAGE' \
'LINE2:ema_hum#56b4e9:EMA Hum' \
'GPRINT:ema_hum:AVERAGE:       Avg\:%5.2lf %%  ' \
'GPRINT:ema_hum:MAX:Max\:%5.2lf %%  ' \
'GPRINT:ema_hum:MIN:Min\:%5.2lf %%  ' \
'GPRINT:ema_hum:LAST:Now\:%5.2lf %%\n'


rrdtool graph /var/www/environment/images/30d.png -a PNG \
--title="Temperature" \
--start -30d \
${graph_layout} \
'DEF:ema_temp=/opt/environment/environment.rrd:ema_temp:AVERAGE' \
'LINE2:ema_temp#cc79a7:EMA Temp' \
'GPRINT:ema_temp:AVERAGE:       Avg\:%5.2lf °C  ' \
'GPRINT:ema_temp:MAX:Max\:%5.2lf °C  ' \
'GPRINT:ema_temp:MIN:Min\:%5.2lf °C  ' \
'GPRINT:ema_temp:LAST:Now\:%5.2lf °C\n'

rrdtool graph /var/www/environment/images/30d_hum.png -a PNG \
--title="Humidity" \
--start -30d \
${graph_layout} \
'DEF:ema_hum=/opt/environment/environment.rrd:ema_hum:AVERAGE' \
'LINE2:ema_hum#56b4e9:EMA Hum' \
'GPRINT:ema_hum:AVERAGE:       Avg\:%5.2lf %%  ' \
'GPRINT:ema_hum:MAX:Max\:%5.2lf %%  ' \
'GPRINT:ema_hum:MIN:Min\:%5.2lf %%  ' \
'GPRINT:ema_hum:LAST:Now\:%5.2lf %%\n'


rrdtool graph /var/www/environment/images/360d.png -a PNG \
--title="Temperature" \
--start -360d \
${graph_layout} \
'DEF:ema_temp=/opt/environment/environment.rrd:ema_temp:AVERAGE' \
'LINE2:ema_temp#cc79a7:EMA Temp' \
'GPRINT:ema_temp:AVERAGE:       Avg\:%5.2lf °C  ' \
'GPRINT:ema_temp:MAX:Max\:%5.2lf °C  ' \
'GPRINT:ema_temp:MIN:Min\:%5.2lf °C  ' \
'GPRINT:ema_temp:LAST:Now\:%5.2lf °C\n'

rrdtool graph /var/www/environment/images/360d_hum.png -a PNG \
--title="Humidity" \
--start -360d \
${graph_layout} \
'DEF:ema_hum=/opt/environment/environment.rrd:ema_hum:AVERAGE' \
'LINE2:ema_hum#56b4e9:EMA Hum' \
'GPRINT:ema_hum:AVERAGE:       Avg\:%5.2lf %%  ' \
'GPRINT:ema_hum:MAX:Max\:%5.2lf %%  ' \
'GPRINT:ema_hum:MIN:Min\:%5.2lf %%  ' \
'GPRINT:ema_hum:LAST:Now\:%5.2lf %%\n'
