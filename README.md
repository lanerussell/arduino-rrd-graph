# Arduino RRD Graph

A simple python script and Arduino project which stores sensor data in a RRD (round robin database).

As configured, this project uses an Arduino Nano (Elegoo) with a SHT31-D temperature and humidity sensor from Adafruit. Data is entered into the RRD every 5 seconds. The RRD graphs are refreshed every 1 minute via cron.

The Arduino sketch depends on the Adafruit_SHT31 library. It also contains some currently unnecessary functions which will someday support control of an HVAC system.


# Getting Started
1. Prepare a user to run all associated shell and python scripts (you can see from the systemd service file that I created a user called `apibot`). This user needs to be added to the `dialout` group
2. Set appropriate directory ownership
3. Run `create_environment_rrd.sh` to create an empty RRD
4. Upload sketch to Arduino, connect to PC, and verify which tty it's using (in my case, `/dev/ttyUSB0`)
5. Update `serial_port` in `environment_sensor.py` as necessary
6. Run `environment_sensor.py` to flush out any issues with permissions, serial connection, etc.
7. If desired, enable and start the systemd service. The service will run on boot, and will restart in the event of any crashes.

![alt text](https://services.pyrahex.com/public/images/git/arduino-rrd-graph/arduino_sensor.jpg)
