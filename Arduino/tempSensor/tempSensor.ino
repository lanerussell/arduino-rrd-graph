#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_SHT31.h"

Adafruit_SHT31 sht31 = Adafruit_SHT31();
int cool_pin = 2;
float cool_set = 27.75;
float ema_alpha = 0.033;
float t = 0.0;
float h = 0.0;
float ema_temp = 0.0;
float ema_hum = 0.0;

float calc_ema(float new_val, float ema) {     //calculate exponential moving average
  ema = (new_val * ema_alpha) + (ema * (1 - ema_alpha)) ;
  return(ema);
}

void control_temp(float temp) {
  if (temp > cool_set) {
    digitalWrite(cool_pin,LOW);
  } else {
    digitalWrite(cool_pin,HIGH);
  }
}

void serial_plot(float y1, float y2, bool plot_row) {
  Serial.print(y1);
  Serial.print(",");
  Serial.print(y2);
  if (! plot_row) {
    Serial.print(",");
  } else {
    Serial.println("");
  }
}

void setup() {
  Serial.begin(9600);

  while (!Serial)
    delay(10);     // will pause until serial console opens

  if (! sht31.begin(0x44)) {   // Set to 0x45 for alternate i2c addr
    Serial.println("Couldn't find SHT31");
    while (1) delay(1);
  }
  
  digitalWrite(cool_pin,HIGH);
  pinMode(cool_pin, OUTPUT);
  
  t = sht31.readTemperature();
  h = sht31.readHumidity();
  
  if (! isnan(t)) {  // check if 'is not a number'
    ema_temp = t;
  }
  if (! isnan(h)) {  // check if 'is not a number'
    ema_hum = h;
  }
}

void send_data() {
  t = sht31.readTemperature();
  h = sht31.readHumidity();

  if (! isnan(t)) {  // check if 'is not a number'
    ema_temp = calc_ema(t, ema_temp);
    control_temp(ema_temp);
    serial_plot(t, ema_temp, false);
  } else { 
    Serial.println("Failed to read temperature");
  }
  
  if (! isnan(h)) {  // check if 'is not a number'
    ema_hum = calc_ema(h, ema_hum);
    serial_plot(h, ema_hum, true);
  } else { 
    Serial.println("Failed to read humidity");
  }
  Serial.println();
}

void loop() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      send_data();
    }
  }
}
